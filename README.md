# Library module
## Running 
- pip install -r requirements.txt
- python manage.py runserver
## Endpoints
| HTTP METHOD | URL            | Description               |
|-------------|----------------|---------------------------|
| GET         | api/category   | all category for learning |
| GET         | api/category/1 | filter category by id     |
| GET         | api/film/      | all films for learning    |
| GET         | api/film/2     | filter films by id        |
| GET         | api/book/      | all books for learning    |
| GET         | api/book/2     | filter books by id        |
| GET         | api/games/     | all games for learning    |
| GET         | api/games/2    | filter games by id       |
| GET         | api/music/     | all music for learning    |
| GET         | api/music/2     | filter music by id       |
