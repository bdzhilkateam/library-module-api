from django.urls import path, include
from rest_framework import routers
from .views import *

router = routers.SimpleRouter()
# router.register(r'content', Content)
router.register(r'category', CategoryList)


urlpatterns = [
    path('api/', include(router.urls)),
    path('api/content/<int:cat_id>/<int:cont_id>', CurrentContent.as_view({'get': 'list'})),
    path('api/content/<int:cat_id>', ContentByCategory.as_view({'get': 'list'})),
]
