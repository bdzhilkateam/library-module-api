# Generated by Django 4.1.7 on 2023-04-24 20:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0003_alter_book_image_alter_category_image_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='film',
            name='trailer',
            field=models.URLField(default='', max_length=255),
        ),
        migrations.CreateModel(
            name='Music',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=255)),
                ('author', models.CharField(max_length=255)),
                ('release', models.CharField(max_length=255)),
                ('image', models.ImageField(default='', upload_to='music/')),
                ('content', models.TextField(blank=True)),
                ('category', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='library.category')),
            ],
        ),
        migrations.CreateModel(
            name='Games',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=255)),
                ('author', models.CharField(max_length=255)),
                ('release', models.CharField(max_length=255)),
                ('image', models.ImageField(default='', upload_to='games/')),
                ('content', models.TextField(blank=True)),
                ('category', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='library.category')),
            ],
        ),
    ]
