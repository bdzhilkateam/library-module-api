from django.shortcuts import render
from django.db import connection
from rest_framework import generics, viewsets

from .serializers import *
from .models import *


class CategoryList(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    print(queryset.query)


class ContentByCategory(viewsets.ReadOnlyModelViewSet):
    serializer_class = ContentSerializer

    def get_queryset(self):
        result = Content.objects.filter(category=self.kwargs["cat_id"])
        print(result.query)
        return result


class CurrentContent(viewsets.ReadOnlyModelViewSet):
    serializer_class = ContentSerializer

    def get_queryset(self):
        result = Content.objects.filter(category=self.kwargs["cat_id"], id=self.kwargs["cont_id"])
        print(result.query)
        return result



