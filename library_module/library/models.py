from django.db.models import JSONField
from django.db import models
from django.urls import reverse


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    cat_name = models.CharField(max_length=255)
    image = models.URLField(max_length=255)

    def __str__(self):
        return self.cat_name


class Content(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    release = models.CharField(max_length=255)
    content = models.TextField(blank=True)
    image = models.URLField(max_length=255)
    trailer = models.URLField(max_length=255, default='')
    words = JSONField()
    category = models.ForeignKey(Category, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.title
